function isNegativeNumber(str) {
 let isNegative = false;
 let number = "";
 for (let char of str) {
  if (char.charCodeAt() === 45) isNegative = true;
 }
 if (isNegative) {
  number = getNumber(str);
 }

 return { isNegative: isNegative, number: number };
}

function getNumber(str) {
 let number = "";
 for (let char of str) {
  if (isANumber(char)) {
   number += char;
  }
 }

 return number;
}

function isANumber(char) {
 // 48 - 57
 if (char.charCodeAt() > 48 && char.charCodeAt() <= 57) return true;
}

function sum(str) {
 if (str.length === 0) return 0;

 let separator = ",";
 if (str[0] === "/" && str[1] === "/") {
  separator = str[2];
  str = str.slice(3);
 }

 let strArray = str.split(separator);

 let total = 0;
 let negativeNumbers = "";

 strArray.forEach((char) => {
  let result = isNegativeNumber(char);
  if (result.isNegative) {
   negativeNumbers += `-${result.number},`;
  } else {
   let number = getNumber(char);
   total += +number;
  }
 });

 if (Boolean(negativeNumbers.length)) throw new Error(`Negative Number found: ${negativeNumbers}`);

 return total;
}

console.log(sum("/n12,34,56,1,2,2,6"));
